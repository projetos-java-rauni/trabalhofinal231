package Log;

public interface LogFormat {
    String formatLog(String operation, String name, String user);
}
